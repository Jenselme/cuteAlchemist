# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/jenselme/projects/cuteAlquemist/cuteAlchemist/connect.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        Dialog.setSizeGripEnabled(True)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(30, 240, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.address = QtWidgets.QLineEdit(Dialog)
        self.address.setGeometry(QtCore.QRect(150, 80, 113, 32))
        self.address.setObjectName("address")
        self.serverType = QtWidgets.QComboBox(Dialog)
        self.serverType.setGeometry(QtCore.QRect(150, 30, 111, 30))
        self.serverType.setObjectName("serverType")
        self.serverType.addItem("")
        self.serverType.addItem("")
        self.serverType.addItem("")
        self.username = QtWidgets.QLineEdit(Dialog)
        self.username.setGeometry(QtCore.QRect(150, 120, 113, 32))
        self.username.setObjectName("username")
        self.password = QtWidgets.QLineEdit(Dialog)
        self.password.setGeometry(QtCore.QRect(150, 160, 113, 32))
        self.password.setObjectName("password")
        self.serverTypeLabel = QtWidgets.QLabel(Dialog)
        self.serverTypeLabel.setGeometry(QtCore.QRect(30, 30, 91, 20))
        self.serverTypeLabel.setToolTipDuration(0)
        self.serverTypeLabel.setObjectName("serverTypeLabel")
        self.serverAddressLabel = QtWidgets.QLabel(Dialog)
        self.serverAddressLabel.setGeometry(QtCore.QRect(25, 90, 101, 20))
        self.serverAddressLabel.setObjectName("serverAddressLabel")
        self.usernameLabel = QtWidgets.QLabel(Dialog)
        self.usernameLabel.setGeometry(QtCore.QRect(30, 130, 66, 20))
        self.usernameLabel.setObjectName("usernameLabel")
        self.passwordLabel = QtWidgets.QLabel(Dialog)
        self.passwordLabel.setGeometry(QtCore.QRect(30, 170, 66, 20))
        self.passwordLabel.setObjectName("passwordLabel")

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.serverType.setItemText(0, _translate("Dialog", "PostgresQL"))
        self.serverType.setItemText(1, _translate("Dialog", "MySQL/MariaDB"))
        self.serverType.setItemText(2, _translate("Dialog", "SQLite"))
        self.serverTypeLabel.setText(_translate("Dialog", "Server type"))
        self.serverAddressLabel.setText(_translate("Dialog", "Server address"))
        self.usernameLabel.setText(_translate("Dialog", "Username"))
        self.passwordLabel.setText(_translate("Dialog", "Password"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

