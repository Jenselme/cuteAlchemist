################################################################################
# This file is part of cuteAlchemist.
#
# cuteAlchemist is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cuteAlchemist is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cuteAlchemist.  If not, see <http://www.gnu.org/licenses/>.
################################################################################


from PyQt5 import QtWidgets
from Ui_main import Ui_MainWindow
from Ui_connect import Ui_Dialog
import sys


class Main:
    def create_connection(self):
        self.dialog = QtWidgets.QDialog()
        self.connectDialog = Ui_Dialog()
        self.connectDialog.setupUi(self.dialog)
        self.connectDialog.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).clicked.connect(self.get_connection_infos)
        self.dialog.show()

    def get_connection_infos(self):
        print(self.connectDialog.address.text())
        print(self.connectDialog.username.text())
        print(self.connectDialog.password.text())
        print(self.connectDialog.serverType.currentText())

    def start(self):
        self.app = QtWidgets.QApplication(sys.argv)
        MainWindow = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(MainWindow)
        self.ui.pushButton.clicked.connect(self.create_connection)
        MainWindow.show()
        sys.exit(self.app.exec_())

if __name__ == "__main__":
    Main().start()
