#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import os

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_suite = True

    def run_tests(self):
        # import here, cause outside the eggs aren't loaded
        import pytest
        pytest.main(self.test_args)


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.rst')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()

requires = [
    'PyMySQL',
    'psycopg2',
    'qt5',
    'SQLAlchemy',
    'pytest',
]

setup(name='cuteAlchemist',
    version='0.1',
    description='QT5 interface to interact with SQL databases (PostgresQL, MySQL, SQLite) thanks to SQLAlchemy.',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        "Programming Language :: Python 3",
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: X11 Applications :: Qt',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Topic :: Database',
        'Topic :: Database :: Front-Ends',
    ],
    author='Julien Enselme',
    author_email='julien.enselme@centrale-marseille.fr',
    url='https://framagit.org/Jenselme/cuteAlchemist',
    keywords='sql database SQLAlchemy PostgresQL MySQL SQLite',
    license='GNU GPLv3+',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='cuteAlchemist',
    install_requires=requires,
    cmdclass={'test': PyTest},
)
