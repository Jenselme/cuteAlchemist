cuteAlquemist
=============

QT5 interface to interact with SQL databases (PostgresQL, MySQL, SQLite) thanks
to SQLAlchemy.
